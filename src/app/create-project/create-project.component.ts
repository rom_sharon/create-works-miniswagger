import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, ReactiveFormsModule, Validators} from '@angular/forms';
import {BsModalRef} from 'ngx-bootstrap';
import {BsModalService} from 'ngx-bootstrap/modal';
import {CreateParamsComponent} from '../create-params/create-params.component';
import {of} from 'rxjs';
import {ProjectsService} from '../projects.service';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {
  apiForm;
  project;

  constructor(private formBuilder: FormBuilder, public bsModalRef: BsModalRef, private modalService: BsModalService,
              private projectsService: ProjectsService) {
  }

  ngOnInit(): void {
    if (this.project) {
      this.createExistingForm(this.project);
    } else {
      this.createEmptyForm();
      document.querySelector('#name').removeAttribute('readonly');
    }
    this.apiForm.valueChanges.subscribe(value => console.log(value));
  }

  convertParamsFromJsonToForm(resourceJsonParams) {
    const params = new FormArray([]);
    for (const resourceParam of resourceJsonParams) {
      let innerParams = new FormArray([]);
      if (resourceParam.innerParams.length) {
        innerParams = this.convertParamsFromJsonToForm(resourceParam.innerParams);
      }
      params.push(this.formBuilder.group({
        name: resourceParam.name,
        type: resourceParam.type,
        required: resourceParam.required,
        pattern: resourceParam.pattern,
        max_element: resourceParam.max_element,
        innerParams
      }));
    }
    return params;
  }

  createExistingForm(project) {
    const projectResources = this.formBuilder.array([]);
    for (const resource of project.resources) {
      const resourceReturnValues = this.formBuilder.array([]);
      for (const returnValue of resource.returnValues) {
        resourceReturnValues.push(this.formBuilder.group({
          code: returnValue.code,
          description: returnValue.description
        }));
      }
      projectResources.push(this.formBuilder.group({
        path: resource.path,
        method: resource.method,
        params: this.convertParamsFromJsonToForm(resource.params),
        returnValues: resourceReturnValues
      }));
    }

    this.apiForm = this.formBuilder.group({
      root: [project.root],
      name: [project.name],
      description: [project.description],
      resources: projectResources
    });
  }

  createEmptyForm() {
    this.apiForm = this.formBuilder.group({
      root: ['', [Validators.required, Validators.pattern('https?://.+')]],
      name: ['', Validators.required],
      description: '',
      resources: this.formBuilder.array([])
    });
    console.log(this.apiForm.value);
  }


  createResourcesArray() {
    return this.formBuilder.group({
      path: '',
      method: '',
      params: new FormArray([]),
      returnValues: new FormArray([])
    });
  }

  createParamsArray() {
    return this.formBuilder.group({
      name: null,
      type: 'string',
      required: false,
      pattern: '',
      max_element: 1
    });
  }

  createReturnValues() {
    return this.formBuilder.group({
      code: null,
      description: null
    });
  }

  getResources(form) {
    return form.controls.resources.controls;
  }

  getParams(form) {
    // return form.controls.params.controls as FormArray;
    return form.get('params') as FormArray;
  }

  getReturnValues(form) {
    return form.controls.returnValues.controls;
  }

  addResource() {
    const control = this.apiForm.get('resources') as FormArray;
    control.push(this.createResourcesArray());
  }
  addReturnValues(i) {
    const control = this.apiForm.get('resources').controls[i].get('returnValues') as FormArray;
    control.push(this.createReturnValues());
  }

  onSubmit(): void {
    console.log('on submit');
    this.projectsService.addProject(this.apiForm.value);
    this.bsModalRef.hide();
  }

  paramComponent(params, i) {
    this.apiForm.get('resources').controls[i].setControl('params', params);
  }
}
