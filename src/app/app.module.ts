import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CreateProjectComponent} from './create-project/create-project.component';
import {DisplayParamsComponent} from './display-params/display-params.component';
import {DisplayResourcesComponent} from './display-resources/display-resources.component';
import {MainComponent} from './main/main.component';
import {AppRoutingModule} from './app-routing.module';
import {AccordionModule, BsDropdownModule, ModalModule, TabsModule} from 'ngx-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DisplayReturnValuesComponent} from './display-return-values/display-return-values.component';
import {CreateParamsComponent} from './create-params/create-params.component';
import {SendComponent} from './send/send.component';
import {HttpClientModule} from '@angular/common/http';
import { SendParamsComponent } from './send-params/send-params.component';


@NgModule({
  declarations: [
    AppComponent,
    CreateProjectComponent,
    DisplayParamsComponent,
    DisplayResourcesComponent,
    MainComponent,
    DisplayReturnValuesComponent,
    CreateParamsComponent,
    SendComponent,
    SendParamsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
