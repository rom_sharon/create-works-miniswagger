import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendParamsComponent } from './send-params.component';

describe('SendParamsComponent', () => {
  let component: SendParamsComponent;
  let fixture: ComponentFixture<SendParamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendParamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendParamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
