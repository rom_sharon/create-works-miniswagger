import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-send-params',
  templateUrl: './send-params.component.html',
  styleUrls: ['./send-params.component.css']
})
export class SendParamsComponent implements OnInit {

  @Input() paramsArray;
  sendForm;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.sendForm = this.paramsArray;
  }

  getParams(form) {
    return this.sendForm.controls.params.controls;
  }

  getArrayParams(param) {
    return param.controls.value.controls;
  }

  addArrayValue(i: number) {
    const maxElement = this.sendForm.get('params').controls[i].get('max_element').value as number;
    const control = this.sendForm.get('params').controls[i].get('value') as FormArray;
    console.log(maxElement);
    console.log(control.length);
    if (maxElement > control.length) {
      console.log(control.value);
      control.push(this.formBuilder.group({
        element: null
      }));
      console.log(control.value);
    }
  }

  getInnerParam(i) {
    return this.sendForm.get('params').controls[i].get('innerParams') as FormArray;
  }
}
