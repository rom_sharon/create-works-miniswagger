import {Component, Input, OnInit} from '@angular/core';
import {BsModalService} from 'ngx-bootstrap/modal';
import {SendComponent} from '../send/send.component';

@Component({
  selector: 'app-display-resources',
  templateUrl: './display-resources.component.html',
  styleUrls: ['./display-resources.component.css']
})
export class DisplayResourcesComponent implements OnInit {
  @Input() public resources: Array<JSON>;
  @Input() public root: string;

  constructor(private modalService: BsModalService) {
  }

  ngOnInit(): void {
  }

  send(resource) {
    const initialState = {
      resource,
      root: this.root
    };
    const modalRef = this.modalService.show(SendComponent, {initialState});
  }
}
