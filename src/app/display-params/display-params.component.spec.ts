import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayParamsComponent } from './display-params.component';

describe('DisplayParamsComponent', () => {
  let component: DisplayParamsComponent;
  let fixture: ComponentFixture<DisplayParamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayParamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayParamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
