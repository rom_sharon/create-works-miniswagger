import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-display-params',
  templateUrl: './display-params.component.html',
  styleUrls: ['./display-params.component.css']
})
export class DisplayParamsComponent implements OnInit {
  @Input() public params: Array<JSON>;

  constructor() {
  }

  ngOnInit(): void {
  }

}
