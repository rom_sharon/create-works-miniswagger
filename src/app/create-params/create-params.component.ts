import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Form, FormArray, FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-create-params',
  templateUrl: './create-params.component.html',
  styleUrls: ['./create-params.component.css']
})
export class CreateParamsComponent implements OnInit {
  @Output() paramsListener = new EventEmitter<FormArray>();
  @Input() resourceParams;
  paramsForm;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    if (this.resourceParams) {
      this.paramsForm = this.formBuilder.group({
        params: this.resourceParams
      });
    } else {
      this.paramsForm = this.formBuilder.group({
        params: new FormArray([])
      });
    }
    this.paramsForm.valueChanges.subscribe(value => this.saveParams());
  }

  createParamsArray() {
    return this.formBuilder.group({
      name: null,
      type: 'string',
      required: false,
      pattern: '',
      max_element: 1,
      innerParams: new FormArray([])
    });
  }

  addParams() {
    (this.paramsForm.get('params') as FormArray).push(this.createParamsArray());
  }

  getP() {
    // @ts-ignore
    return this.paramsForm.controls.params.controls;
  }

  saveParams() {
    this.paramsListener.emit(this.paramsForm.get('params') as FormArray);
  }

  paramComponent($event: FormArray, j) {
    this.paramsForm.get('params').controls[j].setControl('innerParams', $event);
  }

  getInnerParams(j: number) {
    return this.paramsForm.get('params').controls[j].get('innerParams') as FormArray;
  }
}
