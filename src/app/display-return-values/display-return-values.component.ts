import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-display-return-values',
  templateUrl: './display-return-values.component.html',
  styleUrls: ['./display-return-values.component.css']
})
export class DisplayReturnValuesComponent implements OnInit {

  @Input() public returnValues: Array<JSON>;

  constructor() {
  }

  ngOnInit(): void {
  }

}
