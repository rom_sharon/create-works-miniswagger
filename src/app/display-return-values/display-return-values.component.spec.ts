import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayReturnValuesComponent } from './display-return-values.component';

describe('DisplayReturnValuesComponent', () => {
  let component: DisplayReturnValuesComponent;
  let fixture: ComponentFixture<DisplayReturnValuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayReturnValuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayReturnValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
