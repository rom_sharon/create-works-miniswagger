import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  projects: JSON;

  constructor() {
  }

  getProjects(): Observable<JSON> {
    console.log('loadProjects');
    let projects = JSON.parse(localStorage.getItem('api'));
    if (!projects) {
      projects = JSON.parse('{}');
    }
    this.projects = projects;
    return of(projects);
  }

  deleteProject(projectName: string): boolean {
    try {
      delete this.projects[projectName];
      localStorage.setItem('api', JSON.stringify(this.projects));
      return true;
    } catch (e) {
      return false;
    }
  }

  addProject(project): boolean {
    try {
      this.projects[project.name] = project;
      localStorage.setItem('api', JSON.stringify(this.projects));
      return true;
    } catch (e) {
      return false;
    }
  }
}
