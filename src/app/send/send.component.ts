import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {BsModalRef} from 'ngx-bootstrap';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.css']
})
export class SendComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, public bsModalRef: BsModalRef, private http: HttpClient) {
  }

  resource;
  sendForm;
  root;


  private static getValuesFromArray(param: any) {
    const values = [];
    for (const item of param.value) {
      values.push(item.element);
    }
    return values.join(',');
  }


  static getValuesFromObject(innerParams: any) {
    const paramsAndValues = [];
    for (const param of innerParams.params) {
      console.log(param);
      if (param.type === 'array') {
        paramsAndValues.push(param.name + ':' + SendComponent.getValuesFromArray(param));
      } else if (param.type === 'object') {
        if (param.innerParams) {
          paramsAndValues.push(param.name + '{' + SendComponent.getValuesFromObject(param.innerParams) + '}');
        }
      } else {
        paramsAndValues.push(param.name + ':' + param.value);
      }
    }
    return paramsAndValues.join(',');
  }

  createForm(params) {
    const resourceParams = this.formBuilder.array([]);
    for (const param of params) {
      const validators = [];
      const validatorsDescription = ['type: ' + param.type];
      if (param.required) {
        validators.push(Validators.required);
        validatorsDescription.push('required');
      }
      if (param.pattern) {
        validators.push(Validators.pattern(param.pattern));
        validatorsDescription.push('pattern: ' + param.pattern);
      }
      if (param.type === 'array') {
        resourceParams.push(this.createArrayParam(param, validatorsDescription));
      } else if (param.type === 'object') {
        resourceParams.push(this.createObjectParam(param, validatorsDescription));
      } else {
        resourceParams.push(this.creatPrimitiveParam(param, validatorsDescription, validators));
      }
    }
    return resourceParams;
  }

  ngOnInit(): void {
    const resourceParams = this.createForm(this.resource.params);
    console.log(resourceParams);
    this.sendForm = this.formBuilder.group({
      params: resourceParams
    });
    this.sendForm.valueChanges.subscribe(value => console.log(value));
    console.log(this.sendForm.value);
  }

  send() {
    console.log('send form value', JSON.stringify(this.sendForm.value));
    const paramsAndValues = [];
    for (const param of this.sendForm.value.params) {
      console.log(param);
      if (param.type === 'array') {
        paramsAndValues.push(param.name + '=' + SendComponent.getValuesFromArray(param));
      } else if (param.type === 'object') {
        paramsAndValues.push(param.name + '={' + SendComponent.getValuesFromObject(param.innerParams) + '}');
      } else {
        paramsAndValues.push(param.name + '=' + param.value);
      }
    }
    const url = this.root + this.resource.path + '?' + paramsAndValues.join('&');
    console.log(url);
    this.http
      .get(/*url*/url)
      .subscribe(
        data => alert('success: ' + data),
        error => alert('failed:' + error.message)
      );
    this.bsModalRef.hide();
  }

  private createArrayParam(param, validatorsDescription) {
    return this.formBuilder.group({
      name: [param.name],
      value: this.formBuilder.array([]),
      properties: validatorsDescription.join(', ') + ', max_element: ' + param.max_element,
      type: param.type,
      max_element: param.max_element
    });
  }

  private createObjectParam(param: any, validatorsDescription: string[]) {
    return this.formBuilder.group({
      name: [param.name],
      value: null,
      properties: validatorsDescription.join(', '),
      type: param.type,
      max_element: param.max_element,
      innerParams: this.formBuilder.group({
        params: this.createForm(param.innerParams)
      })
    });
  }

  private creatPrimitiveParam(param: any, validatorsDescription: string[], validators) {
    return this.formBuilder.group({
      name: [param.name],
      value: [null, validators],
      properties: validatorsDescription.join(', '),
      type: param.type
    });
  }
}
