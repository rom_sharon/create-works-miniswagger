import {Component, OnInit} from '@angular/core';
import {BsModalService, BsModalRef} from 'ngx-bootstrap/modal';
import {Router} from '@angular/router';
import {CreateProjectComponent} from '../create-project/create-project.component';
import {ProjectsService} from '../projects.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  projects: JSON;
  public modalRef: BsModalRef;
  objectKeys;

  constructor(private router: Router, private modalService: BsModalService, private projectsService: ProjectsService) {
  }

  getProjects(): void {
    this.projectsService.getProjects()
      .subscribe(heroes => this.projects = heroes);
  }

  ngOnInit(): void {
    this.getProjects();
    this.objectKeys = Object.keys;
  }

  createNewApi() {
    this.modalService.show(CreateProjectComponent, {class: 'modal-lg'});
  }

  editProject(projectObject: any) {
    const initialState = {
      project: projectObject
    };
    this.modalRef = this.modalService.show(CreateProjectComponent, {initialState, class: 'modal-lg'});
  }

  deleteProject(projectName: string) {
    this.projectsService.deleteProject(projectName);
  }
}
